"use strict";

/*

Асинхронність в самому движку v8 JS як така відсутня. JavaScript являється однопоточним, тобто виконує кожну команду послідовно, переходячи до наступної команди як
тільки буде виконана попередня. Усі асинхронні технологію надаються через web API браузера. Сюди входять таймаут функції, fetch API, XMLHTTPRequest, DOM тощо. Як тільки
движок JS натикається на асинхронну функцію, вона відправляється до WEB API, там проходить її опрацювання і таким чином вона не блокує виконання JS кода в call stack.
Після опрацювання асинхронної функції (наприклад в сет таймаут проходить заданний час, або отримується респонс від сервера за допомогою фетч апі) колбек функція з веб
апі переміщається в одну з черг - мікро або макро тасок. Якщо це колбек з проміса то він відправляється в чергу мікро тасок, яка є приорітетнішою ніж макро черга. В 
іншому випадку колбек функція переміщається в чергу макротасок. Далі евент луп перевіряє че кол стак являється пустим, якщо так то він бере першу кол бек функцію з 
черги мікро тасок, і поміщає її в кол стак, де вона виконується. Проходить так називаємий "евент тік". Далі евент луп знову перевіряє че кол стак пустий, рухається до
черги мікро тасок та перевіряє її, якщо вона пуста то рухається то черги макро тасок і бере кол бек звідти і поміщає його в кол стек, де вона виконується. Кол беки з
евент лістенерів також переміщаються в макро чергу. Таким чином забезпечується безперебійне функціонування джава скрипт кода.

*/

/* !!! Примітка !!!

Дані про континент та про район відсутні в JSON файлі з сервера.

*/

const btn = document.querySelector("#ip-finder"),
  container = document.querySelector(".container"),
  countryDiv = document.querySelector(".info__country"),
  regionDiv = document.querySelector(".info__region"),
  cityDiv = document.querySelector(".info__city");

function getInfo(countryDiv, regionDiv, cityDiv, country, region, city) {
  countryDiv.textContent = `Country: ${country}`;
  regionDiv.textContent = `Region: ${region}`;
  cityDiv.textContent = `City: ${city}`;
}

const sendRequest = async () => {
  try {
    const res = await fetch("https://api.ipify.org/?format=json");
    const { ip } = await res.json();
    const res2 = await fetch(`http://ip-api.com/json/${ip}`);
    const { city, country, regionName } = await res2.json();
    getInfo(countryDiv, regionDiv, cityDiv, country, regionName, city);
    alert("Дані про континент та про район відсутні в JSON файлі з сервера.");
  } catch (err) {
    console.log(err);
  }
};

btn.addEventListener("click", sendRequest);
