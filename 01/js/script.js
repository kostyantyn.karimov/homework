/* Теоретичні питання

1. В джаваскрипті об*єкти можуть наслідувати методи від прототипів. Якщо ми створимо функцію конструктор об*єкта, то їй в відповідність створиться
об*єкт прототип до якого можна звернутись через назву функції конструктора та назву властивості prototype (наприклад Person.prototype). Далі ми можемо
записати в цей об*єкт якийсь метод (наприклад Person.prototype.showName = function () {console.log(this.name)}). Даний метод буде тепер знаходитись в прототипі
об*єктів створених за допомогою функції конструктора Person. При виклику метода, движок джаваскрипта спершу шукає цей метод всередині об*єкта, на якому цей метод
викликається. Якщо джаваскрипт не знаходить цей метод всередині об*єкта то він звертається до прототипу об*єкта і знаходить цей метод там, як в даному прикладі. Ми
можемо звернутись до прототипа об*єкта двома способами: перший це записати ім*я екземляру об*єкта і властивість __proto__. Наприклад (jack.__proto__). Другий спосіб
який вказаний вище це записати ім*я функції конструктора та звернутись до її властивості prototype (Person.prototype). В результаті ми отримаємо один і той самий 
об*єкт прототипу (jack.__proto__ === Person.prototype). Так як сам по собі об*єкт прототипу являється об*єктом, то в нього є свій прототип об*єкту (Object.prototype ===
Person.prototype.__proto__ ). Тобто на вищому рівні в нас є функція коструктор об*єктів джаваскрипта Object. В данної функції конструктор є об*єкт прототип Object.prototype
і в даного об*єкта прототипу є методи які є загальними для усіх об*єктів джаваскрипта. Наприклад у протитипа Object є метод toString(). Ми можемо викликати цей метод
на екземплярі об*єкта Person: jack.toString(). В такому випадку движок джавасприкта спершу шукає метод toString() всередині об*єкта jack і не знаходить його, тому 
рухається на рівень вище до прототипу об*єкта jack - Person.prototype або jack.__proto__ і також не знаходить його там, тому рухається ще вище до прототипу об*єкта
Person.prototype.__proto__ або Object.prototype і знаходить його там та викликає. Таким чином створено ланцюг прототипів, де кінцевою ланкою є Object.prototype. Ми
можемо це перевірити звернувшись до прототипу об*єкту Object.prototype.__proto__ === null. 

2. Метод super() викликається у конструкторі класу нащадка для наслідування властивостей батьківського класу. Наприклад якщо клас Person являється батьківським по 
відношенню до класу Man, та клас Person має властивість name потрібно вказати цю властивість в середині методу super(name) в конструкторі класу нащадка Man.
*/

"use strict";

class Employee {
  constructor(name, age, salary) {
    this.name = name;
    this.age = age;
    this.salary = salary;
  }

  get name() {
    return this._name;
  }
  get age() {
    return this._age;
  }
  get salary() {
    return this._salary;
  }

  set name(name) {
    this._name = name;
  }
  set age(age) {
    this._age = age;
  }
  set salary(salary) {
    this._salary = salary;
  }
}

class Programmer extends Employee {
  constructor(lang, ...args) {
    super(...args);
    this.lang = lang;
  }

  get salary() {
    return this._salary;
  }

  set salary(salary) {
    this._salary = salary * 3;
  }
}

const john = new Programmer(["js", "java"], "john", 29, 2000);
const mark = new Programmer(["python", "c#"], "mark", 35, 3000);
const bob = new Programmer(["c++", "ruby"], "bob", 41, 5000);

console.log(john);
console.log(mark);
console.log(Programmer.prototype.__proto__ === Employee.prototype);
