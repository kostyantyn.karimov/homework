"use strict";

/*
Теоретичне питання:

AJAX – Asynchronous JavaScript and XML. Це технологія яка дозволяє обмінюватись інформацією між браузером та сервером без необхідності перезавантажувати сторінку. 
З назви слідує що використовується асинхронний JavaScript та XML. Асинхронність означає, що код JavaScript виконується не послідовно, оскільки при відпралені запиту
на сервер необхідно, що б пройшов певний час до отримання відповіді від сервера. JavaScript за замовчуванням не очікує відповіді від сервера і продовжує послідовне
виконання наступних команд. Виникає два "потока", один це основний код JavaScripta, та другий поток з асинхронним запитом на сервер. Для роботи з асинхронними запитами вводиться спеціальний об*єкт XMLHttpRequest, та, пізніше Promise. XML – EXtensible Markup Language. Це застарілий формат для обміну даними. На данний момент витіснений json форматом, але історично зберігається в назві. 

*/

const container = document.querySelector(".container");

function addCardContent(id, name, episodeId, openingCrawl) {
  return `
  <p>Episode number: ${id}</p>
        <p>Film name: ${name}</p>
        <p>Summary: </p>
        <ul>
            <li>EpisodeId: ${episodeId}</li>
            <li>Name: ${name}</li>
            <li>Opening crawl: ${openingCrawl}</li>
        </ul>
        <h3>Characters:</h3>
  `;
}

function createElements() {
  const card = document.createElement("card");
  const loader = document.createElement("div");
  const charactersList = document.createElement("ul");
  loader.className = "loader";
  card.className = "card";
  return { card, loader, charactersList };
}

function appendElements(container, charactersList, card, loader) {
  card.appendChild(charactersList);
  charactersList.appendChild(loader);
  container.appendChild(card);
}

function createCharacterLi(name) {
  const li = document.createElement("li");
  li.textContent = name;
  return li;
}

function fetchMovies(url) {
  fetch(url)
    .then((res) => res.json())
    .then((data) => {
      data.forEach(({ id, name, episodeId, openingCrawl, characters }) => {
        const { card, loader, charactersList } = createElements();
        card.innerHTML = addCardContent(id, name, episodeId, openingCrawl);
        appendElements(container, charactersList, card, loader);
        characters.forEach((character) => {
          fetch(`${character}`)
            .then((res) => res.json())
            .then(({ name }) => {
              const li = createCharacterLi(name);
              charactersList.append(li);
              loader.classList.add("hide");
            });
        });
      });
    });
}

fetchMovies("https://ajax.test-danit.com/api/swapi/films");
