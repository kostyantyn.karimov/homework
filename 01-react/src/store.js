import { configureStore } from "@reduxjs/toolkit";

import productsSlice from "./redux/productsSlice";
import favouritesSlice from "./redux/favouritesSlice";
import cartSlice from "./redux/cartSlice";
import modalSlice from "./redux/modalSlice";

const store = configureStore({
  reducer: {
    products: productsSlice,
    favourites: favouritesSlice,
    cart: cartSlice,
    modal: modalSlice,
  },
});

export default store;
