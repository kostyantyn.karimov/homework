import { useEffect } from "react";
import "./App.sass";

import Header from "./components/Header/Header";

import AppRouter from "./AppRouter";
import { useDispatch } from "react-redux";

import { fetchAllProducts } from "./redux/productsSlice";
import { getAllFavourites } from "./redux/favouritesSlice";
import { getAllCartItems } from "./redux/cartSlice";
export function App() {
  const dispatch = useDispatch();

  useEffect(() => {
    dispatch(fetchAllProducts());
    dispatch(getAllFavourites());
    dispatch(getAllCartItems());
  }, [dispatch]);

  return (
    <div className="app">
      <Header />
      <AppRouter />
    </div>
  );
}

export default App;
