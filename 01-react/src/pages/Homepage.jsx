import Slider from "../components/Slider";
import Main from "../components/Main/Main";

import PropTypes from "prop-types";
import AddToCard from "../components/Modal/AddToCard";

function Homepage() {
  return (
    <div>
      <Slider />
      <Main></Main>
      <AddToCard />
    </div>
  );
}

Homepage.propTypes = {
  products: PropTypes.array,
  onAddtoFavourite: PropTypes.func,
  onDeleteFromFavourite: PropTypes.func,
  favouriteProducts: PropTypes.array,
  onAddToCard: PropTypes.func,
};

export default Homepage;
