// productsSlice.test.js
import { configureStore } from "@reduxjs/toolkit";
import axios from "axios";
import productsReducer, { fetchAllProducts } from "./productsSlice";

// Mock Axios
jest.mock("axios");

describe("productsSlice", () => {
  let store;

  beforeEach(() => {
    store = configureStore({
      reducer: {
        products: productsReducer,
      },
    });
  });

  it("should handle initial state", () => {
    const state = store.getState().products;
    expect(state).toEqual({
      data: [],
      isLoading: false,
      isError: false,
    });
  });

  it("should handle fetchAllProducts.fulfilled", async () => {
    const products = [
      { id: 1, name: "Product 1" },
      { id: 2, name: "Product 2" },
    ];
    axios.get.mockResolvedValueOnce({ data: products });

    await store.dispatch(fetchAllProducts());

    const state = store.getState().products;
    expect(state.data).toEqual(products);
    expect(state.isLoading).toBe(false);
    expect(state.isError).toBe(false);
  });

  it("should handle fetchAllProducts.pending", () => {
    store.dispatch(fetchAllProducts());

    const state = store.getState().products;
    expect(state.isLoading).toBe(true);
  });

  it("should handle fetchAllProducts.rejected", async () => {
    axios.get.mockRejectedValueOnce(new Error("Failed to fetch products"));

    await store.dispatch(fetchAllProducts());

    const state = store.getState().products;
    expect(state.isLoading).toBe(false);
    expect(state.isError).toBe(true);
  });
});
