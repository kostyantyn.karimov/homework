import cartSlice from "./cartSlice";
import { expect } from "@jest/globals";

describe("cartReducer works", () => {
  test("should get all cart items", () => {
    const state = {
      data: [],
    };
    const action = { type: "cart/getAllCartItems" };
    expect(cartSlice(state, action)).toEqual({ data: [] });
  });

  test("should add cart item", () => {
    const state = {
      data: [],
    };
    const item = {
      productName: "Samsung Galaxy A24 6/128GB Light Green",
      price: "9500",
      src: "https://hotline.ua/img/tx/384/3844227235.jpg",
      id: 4,
      color: "Green",
      count: 1,
    };
    const action = { type: "cart/addToCart", payload: item };
    expect(cartSlice(state, action)).toEqual({
      data: [
        {
          productName: "Samsung Galaxy A24 6/128GB Light Green",
          price: "9500",
          src: "https://hotline.ua/img/tx/384/3844227235.jpg",
          id: 4,
          color: "Green",
          count: 1,
        },
      ],
    });
  });

  test("should remove cart item", () => {
    const state = {
      data: [
        {
          productName: "Samsung Galaxy A24 6/128GB Light Green",
          price: "9500",
          src: "https://hotline.ua/img/tx/384/3844227235.jpg",
          id: 4,
          color: "Green",
          count: 1,
        },
      ],
    };
    const action = { type: "cart/removeFromCart", payload: 4 };
    expect(cartSlice(state, action)).toEqual({
      data: [],
    });
  });

  test("should increase count", () => {
    const state = {
      data: [
        {
          productName: "Samsung Galaxy A24 6/128GB Light Green",
          price: "9500",
          src: "https://hotline.ua/img/tx/384/3844227235.jpg",
          id: 4,
          color: "Green",
          count: 1,
        },
      ],
    };
    const action = { type: "cart/increaseCount", payload: 4 };
    expect(cartSlice(state, action)).toEqual({
      data: [
        {
          productName: "Samsung Galaxy A24 6/128GB Light Green",
          price: "9500",
          src: "https://hotline.ua/img/tx/384/3844227235.jpg",
          id: 4,
          color: "Green",
          count: 2,
        },
      ],
    });
  });

  test("should decrease count", () => {
    const state = {
      data: [
        {
          productName: "Samsung Galaxy A24 6/128GB Light Green",
          price: "9500",
          src: "https://hotline.ua/img/tx/384/3844227235.jpg",
          id: 4,
          color: "Green",
          count: 2,
        },
      ],
    };
    const action = { type: "cart/decreaseCount", payload: 4 };
    expect(cartSlice(state, action)).toEqual({
      data: [
        {
          productName: "Samsung Galaxy A24 6/128GB Light Green",
          price: "9500",
          src: "https://hotline.ua/img/tx/384/3844227235.jpg",
          id: 4,
          color: "Green",
          count: 1,
        },
      ],
    });
  });
  test("should remove all items", () => {
    const state = {
      data: [
        {
          productName: "Samsung Galaxy A24 6/128GB Light Green",
          price: "9500",
          src: "https://hotline.ua/img/tx/384/3844227235.jpg",
          id: 4,
          color: "Green",
          count: 2,
        },
      ],
    };
    const action = { type: "cart/removeAllItems" };
    expect(cartSlice(state, action)).toEqual({
      data: [],
    });
  });
});
