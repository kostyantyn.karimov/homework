import { createSlice } from "@reduxjs/toolkit";

const initialState = {
  data: [],
  isLoading: false,
  isError: false,
};

const favouritesSlice = createSlice({
  name: "favourites",
  initialState,
  reducers: {
    getAllFavourites(state) {
      const favourites = localStorage.getItem("favouriteProducts")
        ? JSON.parse(localStorage.getItem("favouriteProducts"))
        : [];

      state.data = favourites.length
        ? favourites.map((elem) => {
            return { ...elem, isFavourite: true };
          })
        : [];

      localStorage.setItem("favouriteProducts", JSON.stringify(state.data));
    },
    addToFavourites(state, action) {
      if (state.data.find((elem) => elem.id === action.payload.id)) return;
      state.data.push(action.payload);
      localStorage.setItem("favouriteProducts", JSON.stringify(state.data));
    },
    removeFromFavourites(state, action) {
      const index = state.data.findIndex((elem) => elem.id === action.payload);
      if (index !== -1) state.data.splice(index, 1);
      localStorage.setItem("favouriteProducts", JSON.stringify(state.data));
    },
  },
});

console.log(favouritesSlice);

export const { addToFavourites, removeFromFavourites, getAllFavourites } =
  favouritesSlice.actions;

export default favouritesSlice.reducer;
