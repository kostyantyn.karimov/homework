import { createSlice } from "@reduxjs/toolkit";

const initialState = {
  isOpenAdd: false,
  isOpenRemove: false,
  name: "",
  price: 0,
  color: "",
  src: "",
  id: "",
};

const modalSlice = createSlice({
  name: "modal",
  initialState,
  reducers: {
    toggleModalAdd(state) {
      state.isOpenAdd = !state.isOpenAdd;
    },
    toggleModalRemove(state) {
      state.isOpenRemove = !state.isOpenRemove;
    },
    configModal(state, action) {
      state.name = action.payload.name;
      state.price = action.payload.price;
      state.color = action.payload.color;
      state.src = action.payload.src;
      state.id = action.payload.id;
    },
  },
});

export const { toggleModalAdd, toggleModalRemove, configModal } =
  modalSlice.actions;

export default modalSlice.reducer;
