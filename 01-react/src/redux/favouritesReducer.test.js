import favouritesSlice from "./favouritesSlice";
import { expect } from "@jest/globals";

describe("favouriteReducer works", () => {
  test("should get all favourite items", () => {
    const state = {
      data: [],
    };
    const action = { type: "favourites/getAllFavourites" };
    expect(favouritesSlice(state, action)).toEqual({ data: [] });
  });

  test("should add favourite item", () => {
    const state = {
      data: [],
    };
    const item = {
      productName: "Samsung Galaxy A24 6/128GB Light Green",
      price: "9500",
      src: "https://hotline.ua/img/tx/384/3844227235.jpg",
      id: 4,
      color: "Green",
    };
    const action = { type: "favourites/addToFavourites", payload: item };
    expect(favouritesSlice(state, action)).toEqual({
      data: [
        {
          productName: "Samsung Galaxy A24 6/128GB Light Green",
          price: "9500",
          src: "https://hotline.ua/img/tx/384/3844227235.jpg",
          id: 4,
          color: "Green",
        },
      ],
    });
  });

  test("should remove favourite item", () => {
    const state = {
      data: [
        {
          productName: "Samsung Galaxy A24 6/128GB Light Green",
          price: "9500",
          src: "https://hotline.ua/img/tx/384/3844227235.jpg",
          id: 4,
          color: "Green",
        },
      ],
    };
    const action = { type: "favourites/removeFromFavourites", payload: 4 };
    expect(favouritesSlice(state, action)).toEqual({
      data: [],
    });
  });
});
