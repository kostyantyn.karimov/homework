import { useState } from "react";
import s from "./Slider.module.sass";
import { useEffect } from "react";
import { Children } from "react";
import { cloneElement } from "react";
import PropTypes from "prop-types";

function Slider() {
  return (
    <Carousel>
      <div className={s.slideOne}></div>
      <div className={s.slideTwo}></div>
    </Carousel>
  );
}

function Carousel({ children }) {
  const [pages, setPages] = useState([]);
  const [offset, setOffset] = useState(0);
  const [activeSlide, setActiveSlide] = useState(1);

  function handleLeftArrowClick() {
    setOffset((cur) => Math.min(cur + 100, 0));
    setActiveSlide((c) => (c > 1 ? --c : c));
  }

  function handleRightArrowClick() {
    setOffset((cur) => Math.max(cur - 100, -(100 * (pages.length - 1))));
    setActiveSlide((c) => (c === pages.length ? c : ++c));
  }

  useEffect(
    function () {
      setPages(
        Children.map(children, (child) => {
          return cloneElement(child, {
            style: {
              height: "100%",
              minWidth: "100%",
              maxWidth: "100%",
            },
          });
        })
      );
    },
    [children]
  );

  return (
    <div className={s.slider}>
      <div className={s.leftArrow} onClick={handleLeftArrowClick}>
        <svg
          width="24"
          height="45"
          viewBox="0 0 24 45"
          fill="none"
          xmlns="http://www.w3.org/2000/svg"
        >
          <g clipPath="url(#clip0_134_248)">
            <g clipPath="url(#clip1_134_248)">
              <path
                fillRule="evenodd"
                clipRule="evenodd"
                d="M23.4992 43.7723C23.3409 43.9302 23.1528 44.0554 22.9457 44.1408C22.7386 44.2263 22.5166 44.2702 22.2924 44.2702C22.0682 44.2702 21.8462 44.2263 21.6391 44.1408C21.432 44.0554 21.2439 43.9302 21.0856 43.7723L0.631046 23.4352C0.472307 23.2778 0.346367 23.0907 0.260435 22.8848C0.174505 22.6789 0.130272 22.4582 0.130272 22.2353C0.130272 22.0124 0.174505 21.7916 0.260435 21.5857C0.346367 21.3799 0.472307 21.1928 0.631046 21.0354L21.0856 0.698246C21.4057 0.380012 21.8398 0.201233 22.2924 0.201233C22.7451 0.201233 23.1792 0.380012 23.4992 0.698246C23.8193 1.01648 23.9991 1.44809 23.9991 1.89814C23.9991 2.34818 23.8193 2.7798 23.4992 3.09803L4.24809 22.2353L23.4992 41.3726C23.658 41.53 23.7839 41.717 23.8698 41.9229C23.9558 42.1288 24 42.3495 24 42.5725C24 42.7954 23.9558 43.0161 23.8698 43.222C23.7839 43.4279 23.658 43.6149 23.4992 43.7723Z"
                fill="white"
              />
            </g>
          </g>
          <defs>
            <clipPath id="clip0_134_248">
              <rect width="24" height="44.4706" fill="white" />
            </clipPath>
            <clipPath id="clip1_134_248">
              <rect
                width="24"
                height="44.4706"
                fill="white"
                transform="translate(24 44.4706) rotate(-180)"
              />
            </clipPath>
          </defs>
        </svg>
      </div>
      <div className={s.rightArrow} onClick={handleRightArrowClick}>
        <svg
          width="24"
          height="45"
          viewBox="0 0 24 45"
          fill="none"
          xmlns="http://www.w3.org/2000/svg"
        >
          <g clipPath="url(#clip0_134_245)">
            <g clipPath="url(#clip1_134_245)">
              <path
                fillRule="evenodd"
                clipRule="evenodd"
                d="M0.565959 0.698249C0.724296 0.540421 0.912395 0.415202 1.11948 0.329764C1.32657 0.244327 1.54857 0.200348 1.77278 0.200348C1.99698 0.200348 2.21899 0.244327 2.42607 0.329764C2.63316 0.415202 2.82126 0.540421 2.9796 0.698249L23.4341 21.0354C23.5929 21.1928 23.7188 21.3799 23.8048 21.5858C23.8907 21.7917 23.9349 22.0124 23.9349 22.2353C23.9349 22.4582 23.8907 22.6789 23.8048 22.8848C23.7188 23.0907 23.5929 23.2778 23.4341 23.4352L2.9796 43.7724C2.65953 44.0906 2.22542 44.2694 1.77278 44.2694C1.32013 44.2694 0.886027 44.0906 0.565959 43.7724C0.245891 43.4541 0.066078 43.0225 0.066078 42.5725C0.066078 42.1224 0.245891 41.6908 0.565959 41.3726L19.8171 22.2353L0.565959 3.09803C0.407221 2.9406 0.281279 2.75359 0.195347 2.54769C0.109416 2.34179 0.0651855 2.12106 0.0651855 1.89814C0.0651855 1.67522 0.109416 1.45449 0.195347 1.24859C0.281279 1.0427 0.407221 0.855677 0.565959 0.698249Z"
                fill="white"
              />
            </g>
          </g>
          <defs>
            <clipPath id="clip0_134_245">
              <rect width="24" height="44.4706" fill="white" />
            </clipPath>
            <clipPath id="clip1_134_245">
              <rect width="24" height="44.4706" fill="white" />
            </clipPath>
          </defs>
        </svg>
      </div>
      <div className={s.window}>
        <div
          className={s.allSlides}
          style={{ transform: `translateX(${offset}%)` }}
        >
          {pages}
        </div>
      </div>
      <div className={s.pagination}>
        {Array.from({ length: pages.length }, (_, index) => {
          return (
            <div
              key={index}
              style={activeSlide === index + 1 ? { opacity: "1" } : {}}
            ></div>
          );
        })}
      </div>
    </div>
  );
}

Carousel.propTypes = {
  children: PropTypes.node,
};

export default Slider;
