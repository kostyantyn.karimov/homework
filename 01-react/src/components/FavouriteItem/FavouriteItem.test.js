import FavouriteItem from "./FavouriteItem";
import { render } from "@testing-library/react";
import { expect } from "@jest/globals";
import store from "../../store";
import { Provider } from "react-redux";

describe("FavouriteItem snapchot testing", () => {
  test("should render FavouriteItem", () => {
    const product = {
      productName: "Samsung Galaxy A24 6/128GB Light Green",
      price: "9500",
      src: "https://hotline.ua/img/tx/384/3844227235.jpg",
      id: 4,
      color: "Green",
    };
    const { asFragment } = render(
      <Provider store={store}>
        <FavouriteItem product={product} />
      </Provider>
    );
    expect(asFragment()).toMatchSnapshot();
  });
});
