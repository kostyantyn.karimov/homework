import { useDispatch } from "react-redux";
import s from "./FavouriteItem.module.sass";

import PropTypes from "prop-types";
import { removeFromFavourites } from "../../redux/favouritesSlice";
import { configModal, toggleModalAdd } from "../../redux/modalSlice";

function FavouriteItem({ product }) {
  const { productName: name, src, id, price, color } = product;

  const dispatch = useDispatch();

  function handleDeleteFavourites() {
    dispatch(removeFromFavourites(id));
  }

  function showModal() {
    dispatch(toggleModalAdd());
    dispatch(configModal({ name, price, color, src, id }));
  }

  return (
    <div className={s.container}>
      <div className={s.img}>
        <img src={src} alt={name} />
      </div>
      <div className={s.productInfo}>
        <div className={s.productHeader}>
          <h3>{name}</h3>
          <div className={s.delete} onClick={handleDeleteFavourites}>
            &#10060;
          </div>
        </div>
        <button className={s.cartBtn} onClick={showModal}>
          Add to cart
        </button>
      </div>
    </div>
  );
}

FavouriteItem.propTypes = {
  product: PropTypes.shape({
    productName: PropTypes.string,
    id: PropTypes.number,
    price: PropTypes.string,
    src: PropTypes.string,
    color: PropTypes.string,
    isFavourite: PropTypes.bool,
    count: PropTypes.number,
  }),
  onDeleteFromFavourite: PropTypes.func,
};

export default FavouriteItem;
