import Button from "./Button";
import { render, screen, fireEvent } from "@testing-library/react";
import { expect } from "@jest/globals";

describe("Button snapshot testing", () => {
  test("should Button render", () => {
    const { asFragment } = render(<Button classNames="btn">Hello</Button>);

    expect(asFragment()).toMatchSnapshot();
  });
});

describe("Button onClick works", () => {
  test("should Button render", () => {
    const callback = jest.fn();
    render(<Button onClick={callback}>Hello</Button>);

    const btn = screen.getByText("Hello");

    fireEvent.click(btn);
    screen.debug();

    expect(callback).toBeCalled();
  });
});
