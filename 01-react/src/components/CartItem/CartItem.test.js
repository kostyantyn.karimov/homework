import CartItem from "./CartItem";
import { render } from "@testing-library/react";
import { expect } from "@jest/globals";
import store from "../../store";
import { Provider } from "react-redux";

describe("CartItem snapchot testing", () => {
  test("should render CartItem", () => {
    const product = {
      productName: "Samsung Galaxy A24 6/128GB Light Green",
      price: "9500",
      src: "https://hotline.ua/img/tx/384/3844227235.jpg",
      id: 4,
      color: "Green",
      count: 3,
    };
    const { asFragment } = render(
      <Provider store={store}>
        <CartItem product={product} />
      </Provider>
    );
    expect(asFragment()).toMatchSnapshot();
  });
});
