import { useEffect, useState } from "react";
import s from "./CartItem.module.sass";

import PropTypes from "prop-types";
import { useDispatch } from "react-redux";
import { decreaseCount, increaseCount } from "../../redux/cartSlice";
import { configModal, toggleModalRemove } from "../../redux/modalSlice";

function CartItem({ product }) {
  const { productName: name, price, src, count, id, color } = product;

  const [productCount, setProductCount] = useState(count);

  const dispatch = useDispatch();

  function handleShowModal() {
    dispatch(toggleModalRemove());
    dispatch(configModal({ name, price, color, src, id }));
  }

  function handleCountIncrease() {
    dispatch(increaseCount(id));
  }

  function handleCountDecrease() {
    dispatch(decreaseCount(id));
  }

  useEffect(() => {
    setProductCount(count);
  }, [count]);

  return (
    <div className={s.container}>
      <div className={s.img}>
        <img src={src} alt={name} />
      </div>
      <div className={s.info}>
        <div className={s.remove} onClick={handleShowModal}>
          &#10060;
        </div>
        <div className={s.details}>
          <div className={s.name}>
            {name}
            <span> x {count}</span>
          </div>
          <div className={s.color}>
            Color: <span>{color}</span>
          </div>
          <div className={s.count}>
            <span onClick={handleCountDecrease}>-</span>
            <span onClick={handleCountIncrease}>+</span>
          </div>
        </div>
        <div className={s.price}>&#8372; {price * count}</div>
      </div>
    </div>
  );

  // return (
  //   <>
  //     <div className={s.container}>
  //       <div className={s.img}>
  //         <img src={src} alt={name} />
  //       </div>
  //       <div className={s.productInfo}>
  //         <div className={s.productHeader}>
  //           <h3>{name}</h3>
  //           <div className={s.delete} onClick={handleShowModal}>
  //             &#10060;
  //           </div>
  //         </div>
  //         <div className={s.productFooter}>
  //           <div className={s.productCount}>
  //             <label>
  //               Count:
  //               <div onClick={handleCountDecrease}>–</div>
  //               <input
  //                 type="number"
  //                 value={productCount}
  //                 onChange={(e) => setProductCount(+e.target.value)}
  //               />
  //               <div onClick={handleCountIncrease}>+</div>
  //             </label>
  //           </div>
  //           <div className={s.productPrice}>Price: {price * count} UAH</div>
  //         </div>
  //       </div>
  //     </div>
  //   </>
  // );
}

CartItem.propTypes = {
  product: PropTypes.shape({
    productName: PropTypes.string,
    id: PropTypes.number,
    price: PropTypes.string,
    src: PropTypes.string,
    color: PropTypes.string,
    isFavourite: PropTypes.bool,
    count: PropTypes.number,
  }),
  setAddedToCard: PropTypes.func,
  onDeleteFromCart: PropTypes.func,
};

export default CartItem;
