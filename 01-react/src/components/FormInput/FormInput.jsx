import { useField } from "formik";
import PropTypes from "prop-types";
import s from "./FormInput.module.sass";
import { PatternFormat } from "react-number-format";

function FormInput(props) {
  const { labelName, name, placeholder, type, isPhone = false } = props;
  const [field, meta] = useField(props);

  const isError = meta.touched && meta.error;

  if (isPhone) {
    return (
      <div className={s.input}>
        <label>{labelName}</label>
        <PatternFormat
          format="+38(###) ### ## ##"
          placeholder="+38 050 111 22 33"
          name={name}
          {...field}
        />
        {isError && <p className={s.error}>{meta.error}</p>}
      </div>
    );
  }

  return (
    <div className={s.input}>
      <label>{labelName}</label>
      <input type={type} name={name} placeholder={placeholder} {...field} />
      {isError && <p className={s.error}>{meta.error}</p>}
    </div>
  );
}

FormInput.propTypes = {
  labelName: PropTypes.string,
  name: PropTypes.string,
  placeholder: PropTypes.string,
  type: PropTypes.string,
  isPhone: PropTypes.bool,
};

export default FormInput;
