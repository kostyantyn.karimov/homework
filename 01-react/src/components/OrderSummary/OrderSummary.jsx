import { useSelector } from "react-redux";
import s from "./OrderSummary.module.sass";
import CartItem from "../CartItem/CartItem";

function OrderSummary() {
  const products = useSelector((store) => store.cart.data);
  const totalPrice = products.reduce(
    (total, curr) => total + curr.count * Number(curr.price),
    0
  );
  return (
    <div className={s.container}>
      <div className={s.header}>Order Summary</div>
      <div className={s.items}>
        {products.map((product) => (
          <CartItem key={product.id} product={product} />
        ))}
      </div>
      <div className={s.subtotal}>
        <span>
          Subtotal
          <span className={s.itemsCount}>
            {" "}
            ({products.length} {products.length === 1 ? "item" : "items"})
          </span>
        </span>
        <span> &#8372;{totalPrice}</span>
      </div>
      <div className={s.shipping}>
        <span>Shipping</span>
        {totalPrice === 0 ? <span>&#8372;0</span> : <span>-&#8372;500</span>}
      </div>
      <div className={s.total}>
        <span>Total</span>
        {totalPrice === 0 ? (
          <span>&#8372;0</span>
        ) : (
          <span>&#8372;{totalPrice + 500}</span>
        )}
      </div>
    </div>
  );
}

export default OrderSummary;
