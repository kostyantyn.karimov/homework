import ModalWrapper from "./ModalWrapper";
import Modal from "./Modal";
import ModalHeader from "./ModalHeader";
import ModalClose from "./ModalClose";
import ModalBody from "./ModalBody";
import ModalFooter from "./ModalFooter";

function AddToFavourite() {
  return (
    <ModalWrapper onShowModal={() => {}}>
      <Modal>
        <ModalHeader>
          <h1>Add Product “NAME”</h1>
        </ModalHeader>
        <ModalClose onClick={() => {}} />
        <ModalBody>Description for you product</ModalBody>
        <ModalFooter firstText="ADD TO FAVORITE" firstClick={() => {}} />
      </Modal>
    </ModalWrapper>
  );
}

export default AddToFavourite;
