import DeleteProduct from "./DeleteProduct.jsx";
import { expect } from "@jest/globals";
import { fireEvent, render, screen } from "@testing-library/react";
import { Provider, useDispatch } from "react-redux";
import store from "../../store.js";
import { toggleModalRemove } from "../../redux/modalSlice.js";

const Wrapper = () => {
  const dispatch = useDispatch();

  return (
    <>
      <button
        onClick={() => {
          dispatch(toggleModalRemove());
        }}
      >
        open
      </button>
      <DeleteProduct />
    </>
  );
};

describe("Modal open/close", () => {
  test("should render on open", () => {
    render(
      <Provider store={store}>
        <Wrapper />
      </Provider>
    );

    const openBtn = screen.getByText("open");
    fireEvent.click(openBtn);
    expect(screen.getByTestId("123")).toBeInTheDocument();
  });
});
