import ModalWrapper from "./ModalWrapper";
import Modal from "./Modal";
import ModalHeader from "./ModalHeader";
import ModalClose from "./ModalClose";
import ModalBody from "./ModalBody";
import ModalFooter from "./ModalFooter";

import PropTypes from "prop-types";
import { useDispatch, useSelector } from "react-redux";
import { toggleModalAdd } from "../../redux/modalSlice";
import { addToCart } from "../../redux/cartSlice";

function AddToCard() {
  const {
    isOpenAdd: isOpen,
    name,
    color,
    price,
    id,
    src,
  } = useSelector((state) => state.modal);

  const product = { productName: name, price, src, id, color };

  const dispatch = useDispatch();

  function handleShowAddToCard() {
    dispatch(toggleModalAdd());
  }

  function handleAddtoCart() {
    dispatch(addToCart(product));
    dispatch(toggleModalAdd());
  }

  if (!isOpen) return null;

  return (
    <ModalWrapper onShowModal={handleShowAddToCard}>
      <Modal>
        <ModalHeader>
          <h1>
            Add Product <span style={{ fontWeight: "600" }}>{name}</span> to the
            card
          </h1>
        </ModalHeader>
        <ModalClose onClick={handleShowAddToCard} />
        <ModalBody>
          <p>Price: {price} UAH</p>
          <p>Color: {color}</p>
        </ModalBody>
        <ModalFooter firstText="Add to card" firstClick={handleAddtoCart} />
      </Modal>
    </ModalWrapper>
  );
}

AddToCard.propTypes = {
  handleShowAddToCard: PropTypes.func,
  name: PropTypes.string,
  price: PropTypes.string,
  color: PropTypes.string,
};

export default AddToCard;
