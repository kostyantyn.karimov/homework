import PropTypes from "prop-types";

import { body } from "./Modal.module.sass";

function ModalBody({ children }) {
  return <div className={body}>{children}</div>;
}

ModalBody.propTypes = {
  children: PropTypes.node,
};

export default ModalBody;
