import Button from "../Button";

import PropTypes from "prop-types";

import { footer } from "./Modal.module.sass";

function ModalFooter({ firstText, secondaryText, firstClick, secondaryClick }) {
  return (
    <div className={footer}>
      {firstText && firstClick && (
        <Button classNames="modal__button" onClick={firstClick}>
          {firstText}
        </Button>
      )}
      {secondaryText && secondaryClick && (
        <Button classNames="modal__button" onClick={secondaryClick}>
          {secondaryText}
        </Button>
      )}
    </div>
  );
}

ModalFooter.propTypes = {
  firstText: PropTypes.string,
  secondaryText: PropTypes.string,
  firstClick: PropTypes.func,
  secondaryClick: PropTypes.func,
};

export default ModalFooter;
