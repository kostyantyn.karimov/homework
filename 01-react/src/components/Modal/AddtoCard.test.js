import AddToCard from "./AddToCard";
import { expect } from "@jest/globals";
import { fireEvent, render, screen } from "@testing-library/react";
import { Provider, useDispatch } from "react-redux";
import store from "../../store.js";
import { toggleModalAdd } from "../../redux/modalSlice";

const Wrapper = () => {
  const dispatch = useDispatch();

  return (
    <>
      <button
        onClick={() => {
          dispatch(toggleModalAdd());
        }}
      >
        open
      </button>
      <AddToCard />
    </>
  );
};

describe("Modal open/close", () => {
  test("should render on open", () => {
    render(
      <Provider store={store}>
        <Wrapper />
      </Provider>
    );

    const openBtn = screen.getByText("open");
    fireEvent.click(openBtn);
    expect(screen.getByTestId("123")).toBeInTheDocument();
  });
});
