import PropTypes from "prop-types";

import { modalWrapper } from "./Modal.module.sass";

function ModalWrapper({ children, onShowModal }) {
  return (
    <div
      data-testid="123"
      className={modalWrapper}
      onClick={(e) => {
        if (e.target.classList[0].includes("modalWrapper")) onShowModal();
      }}
    >
      {children}
    </div>
  );
}

ModalWrapper.propTypes = {
  children: PropTypes.node,
  onShowModal: PropTypes.func,
};

export default ModalWrapper;
