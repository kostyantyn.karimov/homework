import s from "./CheckOutForm.module.sass";
import { Formik, Form } from "formik";
import FormInput from "../FormInput/FormInput";
import { validationSchema } from "./validationSchema";
import { useDispatch, useSelector } from "react-redux";
import { removeAllItems } from "../../redux/cartSlice";

function CheckOutForm() {
  const products = useSelector((store) => store.cart.data);
  const dispatch = useDispatch();
  const initialValues = {
    firstName: "",
    lastName: "",
    country: "",
    company: "",
    street: "",
    apt: "",
    city: "",
    state: "",
    postal: "",
    phone: "",
  };

  function onSubmit(values, helpers) {
    console.log(values);
    helpers.resetForm();
    let str = "Cart Info:";
    products.forEach(({ productName, count, price }, index) => {
      str += `\n ${index + 1}. ${productName}, count: ${count}, price: ${
        price * count
      } UAH`;
    });
    console.log(str);
    dispatch(removeAllItems());
  }

  return (
    <div className={s.container}>
      <div className={s.header}>Billing Details</div>
      <Formik
        initialValues={initialValues}
        onSubmit={onSubmit}
        validationSchema={validationSchema}
      >
        <Form>
          <div className={s.formContainer}>
            <FormInput
              type="text"
              placeholder="First Name"
              labelName="First Name*"
              name="firstName"
            />
            <FormInput
              type="text"
              placeholder="Last Name"
              labelName="Last Name*"
              name="lastName"
            />
            <FormInput
              type="text"
              placeholder="Country / Region"
              labelName="Country / Region*"
              name="country"
            />
            <FormInput
              type="text"
              placeholder="Company (optional)"
              labelName="Company Name"
              name="company"
            />
            <FormInput
              type="text"
              placeholder="House number and street name"
              labelName="Street address*"
              name="street"
            />
            <FormInput
              type="text"
              placeholder="apartment, suite, unit, etc. (optional)"
              labelName="App, suite, unit"
              name="apt"
            />
            <div className={s.address}>
              <FormInput
                type="text"
                placeholder="Town / city"
                labelName="City*"
                name="city"
              />
              <FormInput
                type="text"
                placeholder="State"
                labelName="State*"
                name="state"
              />
              <FormInput
                type="text"
                placeholder="Postal Code"
                labelName="Postal Code*"
                name="postal"
              />
            </div>
            <FormInput
              labelName="Phone number*"
              type="number"
              name="phone"
              isPhone={true}
            />
            {/* <FormInput
              type="text"
              placeholder="Phone"
              labelName="Phone*"
              name="phone"
            /> */}
          </div>
          <button type="submit">Continue to delivery</button>
        </Form>
      </Formik>
    </div>
  );
}

export default CheckOutForm;
