import * as Yup from "yup";

export const validationSchema = Yup.object().shape({
  firstName: Yup.string()
    .min(2, "Too short")
    .max(50, "Too long")
    .required("First Name is required"),
  lastName: Yup.string()
    .min(2, "Too short")
    .max(50, "Too long")
    .required("Second Name is required"),
  country: Yup.string()
    .min(2, "Too short")
    .max(50, "Too long")
    .required("Country is required"),
  street: Yup.string()
    .min(2, "Too short")
    .max(50, "Too long")
    .required("Street is required"),
  city: Yup.string()
    .min(2, "Too short")
    .max(50, "Too long")
    .required("City is required"),
  state: Yup.string()
    .min(2, "Too short")
    .max(50, "Too long")
    .required("State is required"),
  postal: Yup.number()
    .min(1111, "Too short")
    .required("Postal code is required"),
  phone: Yup.string().required("Phone number is required"),
});
