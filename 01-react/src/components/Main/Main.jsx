import s from "./Main.module.sass";

import PropTypes from "prop-types";

import Product from "../Product/Product";
import ListItem from "../ListItem/ListItem";
import { useSelector } from "react-redux";
import Loader from "../../ui/Loader";
import Error from "../../ui/Error";
import { createSelector } from "@reduxjs/toolkit";
import { useProducts } from "../../context/ProductsContext";

function Main() {
  const isLoading = useSelector((store) => store.products.isLoading);
  const isError = useSelector((store) => store.products.isError);
  const { isList, setIsList } = useProducts();

  const selectAllProductsMemoized = createSelector(
    (state) => state.products.data,
    (state) => state.favourites.data,
    (products, favourites) => {
      const itemsToRender = products.map((product) => {
        const index = favourites.findIndex((el) => el.id === product.id);

        if (index !== -1) {
          return { ...product, isFavourite: true };
        }

        return product;
      });

      return itemsToRender;
    }
  );

  const products = useSelector(selectAllProductsMemoized);

  return (
    <main className={s.main}>
      {isLoading && <Loader />}
      {isError && <Error />}
      {!isLoading && !isError && (
        <>
          <h2>Products</h2>
          <div
            className={s.viewSwitcher}
            onClick={() => {
              setIsList((f) => !f);
            }}
          >
            Change view to {isList ? "carts" : "list"}
          </div>
          {isList && (
            <div className={s.listContainer}>
              {products.map((product) => (
                <ListItem key={product.id} product={product} />
              ))}
            </div>
          )}
          {!isList && (
            <div className={s.container}>
              {products.map((product) => (
                <Product key={product.id} product={product} />
              ))}
            </div>
          )}
        </>
      )}
    </main>
  );
}

Main.propTypes = {
  products: PropTypes.array,
  onAddtoFavourite: PropTypes.func,
  onDeleteFromFavourite: PropTypes.func,
  favouriteProducts: PropTypes.array,
  onAddToCard: PropTypes.func,
};

export default Main;
