import { useContext } from "react";
import { useState } from "react";
import { createContext } from "react";
import PropTypes from "prop-types";
const ProductsContext = createContext();

function ProductsProvider({ children }) {
  const [isList, setIsList] = useState(false);

  return (
    <ProductsContext.Provider value={{ isList, setIsList }}>
      {children}
    </ProductsContext.Provider>
  );
}

function useProducts() {
  const context = useContext(ProductsContext);
  if (context === undefined)
    throw new Error("ProductsContext was used outside the ProductsProvider");
  return context;
}

ProductsProvider.propTypes = {
  children: PropTypes.node,
};

export { ProductsProvider, useProducts };
