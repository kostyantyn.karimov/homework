import { Routes, Route } from "react-router-dom";
import PropTypes from "prop-types";

import Homepage from "./pages/Homepage";
import Cart from "./pages/Cart";
import Favourites from "./pages/Favourites";

function AppRouter() {
  return (
    <Routes>
      <Route path="/" element={<Homepage />} />
      <Route path="/cart" element={<Cart />} />
      <Route path="/favourites" element={<Favourites />} />
    </Routes>
  );
}

AppRouter.propTypes = {
  products: PropTypes.array,
  addedToCard: PropTypes.array,
  onAddtoFavourite: PropTypes.func,
  onDeleteFromFavourite: PropTypes.func,
  favouriteProducts: PropTypes.array,
  onAddToCard: PropTypes.func,
  setAddedToCard: PropTypes.func,
  onDeleteFromCart: PropTypes.func,
};

export default AppRouter;
