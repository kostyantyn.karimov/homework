"use strict";

/*

ПРИМІТКА: 

СТВОРЕНІ ПОСТИ НЕ ДОДАЮТЬСЯ ДО СЕРВЕРА, ТОМУ КНОПКА ВИДАЛЕННЯ ТА EDIT НЕ ПРАЦЮЄ, ОСКІЛЬКИ ПРИ ВИДАЛЕННІ ПРОХОДИТЬ ПРОВІРКА СТАТУСА РЕСПОНСА І СЕРВЕР ПОВЕРТАЄ 404
ПОМИЛКУ (НЕ ЗНАЙДЕНО).

*/

const container = document.querySelector(".cards");
const loader = document.querySelector(".loader");
const addPostsBtn = document.querySelector(".header__add-post-btn");
const overlay = document.querySelector(".overlay");

class Card {
  constructor(firstName, secondName, email, heading, text, postId, onEdit) {
    this.firstName = firstName;
    this.secondName = secondName;
    this.email = email;
    this.heading = heading;
    this.text = text;
    this.postId = postId;
    this.onEdit = onEdit;
  }

  cardsContainer = document.createElement("div");
  btnContainer = document.createElement("div");
  userInfo = document.createElement("div");
  close = document.createElement("div");
  edit = document.createElement("div");

  userContent = document.createElement("div");
  userHeading = document.createElement("div");
  userText = document.createElement("div");

  createElements() {
    this.cardsContainer.className = "cards__card";
    this.userInfo.className = "user-info";
    this.btnContainer.className = "user-info__btn-conteiner";
    this.close.className = "user-info__delete-btn";
    this.edit.className = "user-info__edit-btn";
    this.edit.innerHTML =
      '<?xml version="1.0" ?><svg class="feather feather-edit" fill="none" height="24" stroke="currentColor" stroke-linecap="round" stroke-linejoin="round" stroke-width="2" viewBox="0 0 24 24" width="24" xmlns="http://www.w3.org/2000/svg"><path d="M11 4H4a2 2 0 0 0-2 2v14a2 2 0 0 0 2 2h14a2 2 0 0 0 2-2v-7"/><path d="M18.5 2.5a2.121 2.121 0 0 1 3 3L12 15l-4 1 1-4 9.5-9.5z"/></svg>';
    this.userContent.className = "user-content";
    this.userHeading.className = "user-content__heading";
    this.userText.className = "user-content__text";
    this.userHeading.innerText = this.heading;
    this.userText.innerText = this.text;
  }

  addListener() {
    this.close.addEventListener("click", this.delete.bind(this));
    this.edit.addEventListener("click", () => {
      this.onEdit(this);
    });
  }

  render() {
    this.createElements();
    this.addListener();
    this.cardsContainer.innerHTML = "";
    this.userInfo.innerHTML = "";
    this.userInfo.insertAdjacentHTML(
      "afterbegin",
      `
      <div class="user-info__fullname">
        <div class="user-info__name">${this.firstName}</div>
        <div class="user-info__surname">${this.secondName}</div>
      </div>
      <div class="user-info__email">${this.email}</div>
      `
    );
    this.userInfo.append(this.btnContainer);
    this.btnContainer.append(this.edit, this.close);
    this.cardsContainer.append(this.userInfo);
    this.cardsContainer.append(this.userContent);
    this.userContent.append(this.userHeading, this.userText);
    // this.cardsContainer.insertAdjacentHTML(
    //   "beforeend",
    //   `
    // <div class="user-content">
    //   <div class="user-content__heading">${this.heading}</div>
    //   <div class="user-content__text">${this.text}</div>
    // </div>
    // `
    // );
    container.prepend(this.cardsContainer);
  }

  delete() {
    axios
      .delete(`https://ajax.test-danit.com/api/json/posts/${this.postId}`)
      .then(({ status }) => {
        if (status === 200) {
          this.cardsContainer.remove();
        }
      })
      .catch(() => {
        alert(
          "СТВОРЕНІ ПОСТИ НЕ ДОДАЮТЬСЯ ДО СЕРВЕРА, ТОМУ КНОПКА ВИДАЛЕННЯ ТА EDIT НЕ ПРАЦЮЄ, ОСКІЛЬКИ ПРИ ВИДАЛЕННІ ПРОХОДИТЬ ПРОВІРКА СТАТУСА РЕСПОНСА І СЕРВЕР ПОВЕРТАЄ 404 ПОМИЛКУ (НЕ ЗНАЙДЕНО)."
        );
      });
  }
}

class Modal {
  constructor(onSubmit, header) {
    this.onSubmit = onSubmit;
    this.header = header;
  }
  modal = document.createElement("div");
  closeBtn = document.createElement("div");

  inputLabel = document.createElement("label");
  textAreaLabel = document.createElement("label");

  inputsContainer = document.createElement("div");
  input = document.createElement("input");

  textArea = document.createElement("textarea");
  submitBtn = document.createElement("button");

  heading = document.createElement("h2");

  createElements() {
    this.modal.className = "modal";

    this.closeBtn.className = "modal__close-btn";

    this.inputsContainer.className = "modal__inputs";

    this.input.className = "modal__header";
    this.input.name = "header";
    this.input.type = "text";
    this.inputLabel.innerText = "Title";

    this.textArea.className = "modal__text";
    this.textArea.name = "text";
    this.textAreaLabel.innerText = "Text";

    this.submitBtn.className = "modal__submit-btn";
    this.submitBtn.innerText = "Submit";

    this.heading.className = "modal__header";
    this.heading.innerText = this.header;
  }

  addEventListeners() {
    this.closeBtn.addEventListener("click", this.exit.bind(this));
    overlay.addEventListener("click", this.exit.bind(this));
    this.submitBtn.addEventListener("click", () => {
      this.onSubmit(this);
      this.exit();
    });
  }

  render() {
    this.createElements();
    this.addEventListeners();

    this.modal.append(
      this.heading,
      this.closeBtn,
      this.inputsContainer,
      this.submitBtn
    );

    this.inputsContainer.append(this.inputLabel, this.textAreaLabel);

    this.inputLabel.append(this.input);
    this.textAreaLabel.append(this.textArea);
    container.append(this.modal);
    overlay.style.display = "block";
  }
  exit() {
    this.modal.remove();
    overlay.style.display = "none";
  }
}

function onSubmit() {
  axios
    .post("https://ajax.test-danit.com/api/json/posts", {
      userId: 1,
      title: this.input.value,
      body: this.textArea.value,
    })
    .then(({ data: { id, userId, title, body }, status }) => {
      if (status === 200) {
        axios
          .get("https://ajax.test-danit.com/api/json/users")
          .then(({ data: usersData }) => {
            const { name, email } = usersData.find(({ id }) => id === userId);
            const [firstName, secondName] = name.split(" ");
            new Card(
              firstName,
              secondName,
              email,
              title,
              body,
              id,
              onEdit
            ).render();
          });
      }
    });
}

function onEdit(card) {
  new Modal(onEditSubmit, "Edit post").render();
  function onEditSubmit(modal) {
    axios
      .put(`https://ajax.test-danit.com/api/json/posts/${card.postId}`, {
        userId: card.userId,
        title: modal.input.value,
        body: modal.textArea.value,
      })
      .then(({ status }) => {
        if (status === 200) {
          card.heading = modal.input.value;
          card.text = modal.textArea.value;
          card.createElements();
        }
      })
      .catch(() => {
        alert(
          "СТВОРЕНІ ПОСТИ НЕ ДОДАЮТЬСЯ ДО СЕРВЕРА, ТОМУ КНОПКА ВИДАЛЕННЯ ТА EDIT НЕ ПРАЦЮЄ, ОСКІЛЬКИ ПРИ ВИДАЛЕННІ ПРОХОДИТЬ ПРОВІРКА СТАТУСА РЕСПОНСА І СЕРВЕР ПОВЕРТАЄ 404 ПОМИЛКУ (НЕ ЗНАЙДЕНО)."
        );
      });
  }
}

addPostsBtn.addEventListener("click", () => {
  new Modal(onSubmit, "Create new post").render();
});

axios
  .get("https://ajax.test-danit.com/api/json/users")
  .then(({ data: usersData }) => {
    axios
      .get("https://ajax.test-danit.com/api/json/posts")
      .then(({ data: postsData }) => {
        loader.style.display = "none";
        postsData.forEach(({ id, userId, title, body }) => {
          const { name, email } = usersData.find(({ id }) => id === userId);
          const [firstName, secondName] = name.split(" ");
          new Card(
            firstName,
            secondName,
            email,
            title,
            body,
            id,
            onEdit
          ).render();
        });
      });
  });
